Befor you start with this CRUD Please follow the instructions given bellow.

1. First of all you should config the backkend. To do that, open cmd in your backend root path (Ex: D:\node-express-mongodb-backend) and do a "npm install"

2. Then put the database URL in app/config/DB.Config.js

	Ex: mongodb://localhost:27017/employee_details
	
3. Then go to the root folder using cmd and run "node server.js" command to serve.

	Ex:D:\node-express-mongodb-backend>node server.js

4. Server will run in port 8080 after connecting successfully.

5. After configuring the backend, Please go to the root folder of frontend then open cmd in your root path (Ex: D:\angular-frontend) and do a "npm install"

6. Then run "ng serve --port 8087" command to serve.

	Ex:D:\node-express-mongodb-backend>ng serve --port 8087

7. Server will run in port 8087 after connecting successfully.

8. End of the successful configuration you can open the project locally using "http://localhost:8087/employees"

9. Available Functionalities

		#Add Employee Details
		#When you are adding the details, all the fields are required and NIC number can not be duplicated.
		#view all the employees names in the landing page
		#once you click on the employee name, you can see the details of each and every employee
		#update the employee details 
		#Delete employee details one by one (Deactivate the Account using a status. 1 - Active, 0- Deactive)
		#Delete all employees details (Deactivate the Account using a status. 1 - Active, 0- Deactive)
		(Both delete functions do the soft delete but if want, there are two seperate APIs to remove records from the database.)
