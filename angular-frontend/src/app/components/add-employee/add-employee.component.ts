import { Component, OnInit } from '@angular/core';
import { EmployeeService } from 'src/app/services/employee.service';


@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css'],

})
export class AddEmployeeComponent implements OnInit {
 message = '';
  employee = {
    name: '',
    age: '',
    date_of_birth: '',
    bio: '',
    nic: '',
    status: ''
  };
  submitted = false;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit() {
   this.message = '';
  }

  saveEmployee() {
    const data = {
      name: this.employee.name,
      age: this.employee.age,
       date_of_birth: this.employee.date_of_birth,
        bio: this.employee.bio,
         nic: this.employee.nic,
         status: 1
    };

    this.employeeService.create(data)
      .subscribe(
        response => {
          console.log(response);
          this.message = (response['message']);
          this.submitted = true;
        },
        error => {
          console.log(error);
        });
  }

  newEmployee() {
    this.submitted = false;
    this.employee = {
      name: '',
      age: '',
      date_of_birth: '',
      bio: '',
      nic: '',
      status: ''
    };
  }

}
