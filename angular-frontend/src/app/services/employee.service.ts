import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = 'http://localhost:8080/api/employees';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getAll() {
    return this.http.get(baseUrl);
  }

  get(id) {
    return this.http.get(`${baseUrl}/${id}`);
  }

  create(data) {
    return this.http.post(baseUrl, data);
  }

  update(id, data) {
    return this.http.put(`${baseUrl}/${id}`, data);
  }

  deactivate(id,status) {
    return this.http.put(`${baseUrl}/deactivate/${id}`,status);
  }

  deactivateAll() {
    return this.http.put(baseUrl,status);
  }

  findByTitle(title) {
    return this.http.get(`${baseUrl}/search?name=${title}`);
  }
}
