module.exports = app => {
  const employees = require("../controllers/employee.controller.js");

  var router = require("express").Router();

  // Create a new Employee
  router.post("/", employees.create);

  // Retrieve all Employees
  router.get("/", employees.findAll);

// Retrieve all Employees by name
  router.get("/search", employees.findAllName);

  // Retrieve a single Employee with id
  router.get("/:id", employees.findOne);

  // Update a Employee with id
  router.put("/:id", employees.update);

  // Deactive a Employee by the id in the request
  router.put("/deactivate/:id", employees.deactivate);

  // Deactive all Employees
  router.put("/", employees.deactivateAll);

  // Delete All
  router.delete("/", employees.deleteAll);

  // Delete an Employee
  router.delete("/:id", employees.delete);

  app.use("/api/employees", router);
};
