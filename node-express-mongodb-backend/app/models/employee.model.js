module.exports = mongoose => {
  
  var schema = mongoose.Schema(
    {
      name: String,
      age: Number,
      date_of_birth: Date,
      nic:String,
      bio: String,
      status: Number
    },
    { timestamps: true }
  );

  schema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });

  const Employee = mongoose.model("employee", schema);
  return Employee;
};
