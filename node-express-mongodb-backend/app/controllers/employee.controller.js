const db = require("../models");
const Employee = db.employees;
const activeUser = 1;
const deactivateUser = 0;


// Create and Save a new Employee
exports.create = (req, res) => {
  // Validate request. We can validate other entities as well.
  if (!req.body.name) {
    res.send({ message: "Name can not be empty!" });
    return;
  }

  if (!req.body.date_of_birth) {
    res.send({ message: "Date of Birth can not be empty!" });
    return;
  }


  if (!req.body.nic) {
    res.send({ message: "NIC can not be empty!" });
    return;
  }


  // check for duplicate records using nic

  var nic = req.body.nic;
  Employee.find({ nic: nic.toString() })
    .then(data => {
      if(data != ''){

       res.send({ message: "Duplicate NIC found. Please try again!" });
       return;
      }
      else{

            const employee = new Employee({
        
            name: req.body.name,
            age: req.body.age,
            date_of_birth: req.body.date_of_birth,
            nic: req.body.nic,
            bio: req.body.bio,
            status: activeUser// register an Employee as a Active User
          });

            // Save Employee in the database
            employee
              .save(employee)
              .then(data => {
                res.send({ message: "You submitted successfully!" });
                return;
              })
              .catch(err => {
                res.status(500).send({
                  message:
                    err.message || "Some error occurred while creating the Employee."
                });

              });
      }

    });

};



// Retrieve all Employees from the database.
exports.findAll = (req, res) => {
  
  Employee.find({ status: 1})
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving employees."
      });
    });
};

//search by name

exports.findAllName = (req, res) => {
   const name = req.query.name;
    var condition = name ? { name: { $regex: new RegExp(name), $options: "i" } } : {};

  Employee.find(condition)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving employees."
      });
    });
};


// Find a single Employee with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Employee.findById(id)
    .then(data => {
      if(!data)
        res.status(404).send({ message: "Not found Employee with id " + id });
      else res.send(data);
      
    })
    .catch(err => {
      res
        .status(500)
        .send({ message: "Error retrieving Employee with id=" + id });
    });
};



// Update a Employee by the id in the request
exports.update = (req, res) => {

   // Validate request. We can validate other entities as well.
  if (!req.body.name) {
    res.send({ message: "Name can not be empty!" });
    return;
  }

  if (!req.body.date_of_birth) {
    res.send({ message: "Date of Birth can not be empty!" });
    return;
  }


  if (!req.body.nic) {
    res.send({ message: "NIC can not be empty!" });
    return;
  }

  const id = req.params.id;

  Employee.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot update Employee with id=${id}. Maybe Employee was not found!`
        });
      } else res.send({ message: "Employee was updated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Employee with id=" + id
      });
    });
};



// Deactive a Employee by the id in the request
exports.deactivate = (req, res) => {
  if (!req.body) {
    return res.status(400).send({
      message: "Data to update can not be empty!"
    });
  }

  const id = req.params.id;

  Employee.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot deactivate the Account with id=${id}. Maybe Employee was not found!`
        });
      } else res.send({ message: "Account was deactivated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error Deactivating Account with id=" + id
      });
    });
};


// Deactive all Employees by the id in the request
exports.deactivateAll = (req, res) => {
  
  Employee.updateMany(
   { $set: { status: deactivateUser } }, // Deactivate status of Employee is 0.
    )

    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot deactivate the Account with id=${id}. Maybe Employee was not found!`
        });
      } else res.send({ message: "Accounts were deactivated successfully." });
    })
    .catch(err => {
      res.status(500).send({
        message: "Error Deactivating Account with id=" + id
      });
    });
};



// Delete an Employee with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Employee.findByIdAndRemove(id, { useFindAndModify: false })
    .then(data => {
      if (!data) {
        res.status(404).send({
          message: `Cannot delete Employee with id=${id}. Maybe Employee was not found!`
        });
      } else {
        res.send({
          message: "Employee was deleted successfully!"
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Employee with id=" + id
      });
    });
};


// Delete all Employees from the database.
exports.deleteAll = (req, res) => {
  Employee.deleteMany({})
    .then(data => {
      res.send({
        message: `${data.deletedCount} Employees were deleted successfully!`
      });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all employees."
      });
    });
};

